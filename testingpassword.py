#!/usr/bin/python3.6

import josepalaciosinterview

def run_n_times() -> None:
    for i in range(4):
        print("---------------------------------")
        print("Testing Complexity %s" % str(i+1))
        print("---------------------------------")
        for j in range(1,20): 
            passwd=josepalaciosinterview.generate_password(j,i+1)
            print(passwd)
            if len(passwd)>0:
                check=josepalaciosinterview.check_password_level(passwd) 
                print("Complexity  %s" % str(check)) 


if __name__ == "__main__":
    run_n_times()



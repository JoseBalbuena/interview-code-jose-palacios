#!/usr/bin/python3.6

import random   # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
import string   # https://docs.python.org/3.6/library/string.html
import re       # https://docs.python.org/3/library/re.html
import requests # https://http://docs.python-requests.org/en/master/
import json     # https://docs.python.org/3/library/json.html
from sqlite3 import Error


def ramdompass(lowercaselength: int,digitslength: int,uppercaselength: int,ponctuationlength: int) -> str: 
    """ Function to help to generate random password
    """
    randompassword=""
    letters_l=string.ascii_lowercase
    letters_u=string.ascii_uppercase
    digits=string.digits
    punctuation=string.punctuation
    for i in range(lowercaselength):
        randompassword=random.choice(letters_l) + randompassword
    for i in range(digitslength):
        randompassword=randompassword + random.choice(digits)
    for i in range(uppercaselength):
        randompassword=randompassword + random.choice(letters_u)
    for i in range(ponctuationlength):
        randompassword=randompassword + random.choice(punctuation)
    randompasswordlist=list(randompassword)
    random.shuffle(randompasswordlist)
    randompassword=''.join(randompasswordlist)
    return randompassword


def generate_password(length: int, complexity: int) -> str:
    """Generate a random password with given length and complexity

    Complexity levels:
        Complexity == 1: return a password with only lowercase chars
        Complexity ==  2: Previous level plus at least 1 digit
        Complexity ==  3: Previous levels plus at least 1 uppercase char
        Complexity ==  4: Previous levels plus at least 1 punctuation char

    :param length: number of characters
    :param complexity: complexity level
    :returns: generated password
    """
    randompassword=""
    letters_l=string.ascii_lowercase
    letters_u=string.ascii_uppercase
    digits=string.digits
    punctuation=string.punctuation  
    if complexity==1:
        lowercaselength=length
        digitslength=0
        uppercaselength=0
        ponctuationlength=0
        randompassword=ramdompass(lowercaselength,digitslength,uppercaselength,ponctuationlength)
    elif complexity==2:
       #complexity==2 and length=1 is an invalid combination 
        if length>1:
            #At least 1 digit
            lowercaselength=random.randint(1,length-1)
            digitslength=length-lowercaselength
            uppercaselength=0
            ponctuationlength=0
            randompassword=ramdompass(lowercaselength,digitslength,uppercaselength,ponctuationlength)
        else: 
             print("Please enter a lenght > 1")
    elif complexity==3:
       #complexity==3 and length=2 is an invalid combination 
        if length>2:
            #At least 1 digit and 1 UpperCase
            lowercaselength=random.randint(1,length-2)
            digitslength=random.randint(1,length-lowercaselength-1)
            uppercaselength=length-lowercaselength-digitslength
            ponctuationlength=0
            randompassword=ramdompass(lowercaselength,digitslength,uppercaselength,ponctuationlength)
        else:
            print("Please enter a lenght > 2")
    elif complexity==4:
       #complexity==4 and length=3 is an invalid combination 
        if length>3:
            #At least 1 digit, 1 UpperCase, and 1 ponctuation
            lowercaselength=random.randint(1,length-3)
            digitslength=random.randint(1,length-lowercaselength-2)
            uppercaselength=random.randint(1,length-lowercaselength-digitslength-1)
            ponctuationlength=length-lowercaselength-digitslength-uppercaselength
            randompassword=ramdompass(lowercaselength,digitslength,uppercaselength,ponctuationlength)
        else:
            print("Please enter a lenght > 3")

    else:
        print("Please enter a level of complexity between 1-4")

    return randompassword

def check_password_level(password: str) -> int:
    """Return the password complexity level for a given password

    Complexity levels:
        Return complexity 1: If password has only lowercase chars
        Return complexity 2: Previous level condition and at least 1 digit
        Return complexity 3: Previous levels condition and at least 1 uppercase char
        Return complexity 4: Previous levels condition and at least 1 punctuation

    Complexity level exceptions (override previous results):
        Return complexity 2: password has length >= 8 chars and only lowercase chars
        Return complexity 3: password has length >= 8 chars and only lowercase and digits

    :param password: password
    :returns: complexity level
    """
    #Only lowercase
    if re.match('^[a-z]+$',password):
       complexity=1
       #password >=8 chars
       if len(password)>=8:
           complexity=2
    #Lowercase and digits
    elif re.match('^[a-z0-9]+$',password):
        complexity=2 
        #password >= 8chars
        if len(password)>=8:
            complexity=3
    #Lowercase, UpperCase and Digits
    elif re.match('^[A-Za-z0-9]+$',password):
     complexity=3
    else:
     complexity=4
    return complexity

def create_user(db_path: str) -> None:  # you may want to use: http://docs.python-requests.org/en/master/
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db

    :param db_path: path of the SQLite db file (to do: sqlite3.connect(db_path))
    :return: None
    """
    #Getting Data from webpage
    r = requests.get('https://randomuser.me/api/')
    randomuser = r.json()
    first=randomuser['results'][0]['name']['first']
    last=randomuser['results'][0]['name']['last']
    email=randomuser['results'][0]['email']
    #length
    length=random.randint(6,12)
    complexity=random.randint(1,4)
    password=generate_password(length,complexity)
    #Creating Sqlite DB and Table
    try:
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        query='CREATE TABLE IF NOT EXISTS randomuser(fullname TEXT,password TEXT,email TEXT PRIMARY KEY)'
        c.execute(query);
        query="INSERT INTO randomuser(fullname,password,email) VALUES('" + first + " " + last + "','" + password + "','" + email + "')"
        c.execute(query)
        conn.commit()
    except Error as e:
        print(e)
    finally: 
        conn.close()

if __name__ == "__main__":
 passw=generate_password(10,2)
 print(passw)
 complexity=check_password_level(passw)
 print(complexity)
 create_user("/tmp/teste.db")
 

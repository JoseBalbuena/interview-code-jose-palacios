














[jose@rejane interview-code-jose-palacios]$ ./saveuserdb.py 
[jose@rejane interview-code-jose-palacios]$ sqlite3 /tmp/testejose.db 
SQLite version 3.7.17 2013-05-20 00:56:22
Enter ".help" for instructions
Enter SQL statements terminated with a ";"
sqlite> select * from randomuser;
basil dumont|7164f58|basil.dumont@example.com
matthew patel|m5mC506A|matthew.patel@example.com
timmothy hudson|1512i2|timmothy.hudson@example.com
caleb thomas|vP>}83b919$c|caleb.thomas@example.com
evie davies|ookzdwl|evie.davies@example.com
torbjørn gaarder|xshc2vtttx|torbjørn.gaarder@example.com
betina evensen|08W6-kYJ|betina.evensen@example.com
stephanie rivera|jomiizczl|stephanie.rivera@example.com
ayse bremer|335umPy0|ayse.bremer@example.com
villads christensen|rnqfnjtxq|villads.christensen@example.com
sqlite> .exit
[jose@rejane interview-code-jose-palacios]$ 



## Solution

Three scripts were created.

1. josepalaciosinterview.py, this script contain all the functions requested.
2. testingpassword.py, this script will generate a lot of random password whith differente complexities and lengths.
3. saveuserdb.py, this script will store data from randomuser api, create a password and store in sqlite database.

The requirements were:

1. complexity=1 all lowercases.
2. complexity=2 at least 1 digit => Supposed a password with lenght=n and length(digits)=m then lengh(lowercases)=n-m, where lengh(lowercases)=n-1 as maximum. Example if n=10 then length(lowercases) can be in range [1,9](because i need at least 1 digit char), supposed lengh(lowercase)=5  then length(digits)=10-5=5 
3. complexity=3 same as Complexity=2 and at least 1 UpperCase => Supposed a password lenght=n, length(digits)=m and length(uppercases)=p, then lenght(lowercases)=n-m-p where lengh(lowercases)=n-2 as maximum value. Supposed n=10, lenght(lowercases) can be in range (1,8)(because i need at least 1 digit and 1 uppercase), supposed lenght(lowercase)=5 then length(digits) can be (1-4)(because i need at least 1 uppercase), supposed length(digits)=2 then lenght(uppercase)=10-5-2=3
4. complexity=4 same as Complexity=3 and at least 1 Puntuaction => Supposed a password lenght=n, length(digits)=m, length(uppercases)=p and length(punctuation)=q then lenght(lowercases)=n-m-p-q where lengh(lowercases)=n-3 as maximum value. Supposed n=10, lenght(lowercases) can be (1-7)(because i need at least 1 digit, 1 uppercase and 1 punctuation), supposed leght(lowercase)=3 then length(digits) can be (1-5)(because i need at leat 1 Uppercase an 1 punctuation), supposed length(digits)=2 then lenght(uppercase) can be (1-4)(because  i need at least 1 punctuation) supposed lenght(uppercase)=3 then length(puntuaction)=10-3-2-3=2 

With the previous logic we developed an auxiliar function called randompass, randompass also will shuffle the chars in password.

Some combinations are not valid for example, complexity=2 with lenght=1, complexity=3 with lenght=2.

To check password complexity we use regexp.

To collect data from randomuser API we use a requests python module.

Im uploading the log of scripts.

